from flask import Flask, jsonify, abort, request
from flask_sqlalchemy import SQLAlchemy
import datetime
import json

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:password@db/expenses'
db = SQLAlchemy(app)

class Expenses(db.Model):
        __tablename__ = 'Exp'
        id = db.Column(db.Integer, primary_key=True)
        name = db.Column(db.String(20))
        email = db.Column(db.String(30))
        category = db.Column(db.String(100))
        description = db.Column(db.String(20))
        link = db.Column(db.String(100))
        estimated_costs = db.Column(db.String(30))
        submit_date = db.Column(db.String(10))
        status = db.Column(db.String(10))
        decision_date = db.Column(db.String(10))

        def __init__(self, name, email, category,description, link, estimated_costs, submit_date,status,decision_date):
            self.name = name
            self.email = email
            self.category = category
            self.description = description
            self.link = link
            self.estimated_costs = estimated_costs
            self.submit_date = submit_date
            
        @property
        def serialize(self):
            """Return object data in easily serializeable format"""
            return {
                 'id': self.id, 
                 'name': self.name,
                 'email': self.email,
                 'category': self.category,
                 'description': self.description,
                 'link': self.link,
                 'estimated_costs': self.estimated_costs,
                 'submit_date': self.submit_date,
                 'status': self.status,
                 'decision_date': self.decision_date
          }    
                                
            

@app.route('/v1/expenses', methods = ['GET'])
def index():
        return jsonify(Expenses=[i.serialize for i in Expenses.query.all()])

@app.route('/v1/expenses/<int:id>', methods = ['GET'])
def get_exp(id):

        exp = Expenses.query.get(id)
        if exp == None:
            return json.dumps({}), 404
        else:     
            exp_new = {'id': exp.id, 
                    'name': exp.name,
                    'email': exp.email,
                    'category': exp.category,
                    'description': exp.description,
                    'link': exp.link,
                    'estimated_costs': exp.estimated_costs,
                    'submit_date': exp.submit_date,
                    'status': exp.status,
                    'decision_date': exp.decision_date}
            return json.dumps(exp_new)       
        
       



@app.route('/v1/expenses', methods = ['POST'])
def create_exp():
        
        exp = Expenses('name','email', 'category','description', 'link', 'estimated_costs', 'submit_date','status','decision_date')
        data = json.loads(request.data)
        exp.name=data['name']
        exp.email=data['email']
        exp.category=data['category']
        exp.description=data['description']
        exp.link=data['link']
        exp.estimated_costs=data['estimated_costs']
        exp.submit_date=data['submit_date']
        exp.status='pending'
        exp.decision_date=""
        db.session.add(exp)
        db.session.commit()
        exp_new = {'id': exp.id, 
                 'name': exp.name,
                 'email': exp.email,
                 'category': exp.category,
                 'description': exp.description,
                 'link': exp.link,
                 'estimated_costs': exp.estimated_costs,
                 'submit_date': exp.submit_date,
                 'status': exp.status,
                 'decision_date': exp.decision_date}
        return json.dumps(exp_new), 201


@app.route('/v1/expenses/<int:id>', methods = ['DELETE'])
def delete_exp(id):
        db.session.delete(Expenses.query.get(id))
        db.session.commit()
        return json.dumps( { 'result': True } ), 204

@app.route('/v1/expenses/<int:id>', methods = ['PUT'])
def update_exp(id):
        exp = Expenses.query.get(id)
        data = json.loads(request.data)
        if  request.data and 'estimated_costs' in request.data:
            exp.estimated_costs=data['estimated_costs']
        
        db.session.commit()
        
        return json.dumps({}), 202


if __name__ == "__main__":
    db.create_all()
    app.run(debug=True,host='0.0.0.0')
